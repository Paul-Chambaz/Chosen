# Chosen

## About

A simple way to see who goes first at board games.

## Build

Download processing 4.
Then download the android processing plugin.
Finally run **File/Export Signed Package**.

You can now install this package on any android phone.

## Installation

Download the Chosen.apk (either by building it or from the release page).
Then just install it and make sure to allow your browser as a source for installing applications.
You should then just be able to run the app.

## License

This project is licenced under the GPLv2 license.
For more information, read the LICENSE file.
