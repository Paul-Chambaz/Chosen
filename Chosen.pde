/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or 
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY: without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * @file Chosen.pde
 * @author Paul Chambaz
 * @date 18 Aug 2022
 * @brief this very small program chooses a winner using fingers pressed on the screen
 */

import android.view.MotionEvent;

int touch_events;
int current_pointer_id;

float inner_radius;
float outer_radius;
float larger_radius;

int chosen;

class
finger
{
  float x;
  float y;
  color c;
}

float timer = 0;
float time_frame = 0;

ArrayList<finger> fingers;

void
setup ( )
{
  fullScreen();
  fingers = new ArrayList<finger>();
  current_pointer_id = 0;
  chosen = -1;
  inner_radius = min(width, height) * .16;
  outer_radius = min(width, height) * .22;
  larger_radius = outer_radius;
  time_frame = millis();
}

void
draw ( )
{
  colorMode(HSB, 255);
  if (chosen == -1) {
    time_frame = millis() - time_frame;
    if (fingers.size() > 1) {
      timer += time_frame;
    }

    time_frame = millis();

    if (timer > 1000 * 1.5) {
      chosen = floor(random(0, fingers.size()));
    }
    

    background(0);
    for (int i = 0; i < fingers.size(); i++) {
      fill(fingers.get(i).c);
      circle(fingers.get(i).x, fingers.get(i).y, outer_radius);
      fill(0);
      circle(fingers.get(i).x, fingers.get(i).y, inner_radius);
    }
  } else {
    if (larger_radius < 2 * sqrt(2) * max(width, height)) {
      larger_radius *= 1.3;
    }
    background(0);
    if (chosen == -1 || fingers.size() == 0) { return; }
    fill(fingers.get(chosen).c);
    if (chosen == -1 || fingers.size() == 0) { return; }
    circle(fingers.get(chosen).x, fingers.get(chosen).y, larger_radius);
    fill(0);
    if (chosen == -1 || fingers.size() == 0) { return; }
    circle(fingers.get(chosen).x, fingers.get(chosen).y, outer_radius);
    if (chosen == -1 || fingers.size() == 0) { return; }
    fill(fingers.get(chosen).c);
    if (chosen == -1 || fingers.size() == 0) { return; }
    circle(fingers.get(chosen).x, fingers.get(chosen).y, inner_radius);
  }
}

public boolean
surfaceTouchEvent ( MotionEvent event )
{
  if (chosen == -1) {
    touch_events = event.getPointerCount();

    int event_remove = -1;

    if (event.getActionMasked() == 0) {
      fingers.clear();
      finger f = new finger();
      f.c = color(random(0, 255), 255, 255);
      fingers.add(f);
      timer = 0;
    } else if (event.getActionMasked() == 1) {
      event_remove = event.getActionIndex();
      timer = 0;
    } else if (event.getActionMasked() == 5) {
      finger f = new finger();
      f.c = color(random(0, 255), 255, 255);
      fingers.add(f);
      timer = 0;
    } else if (event.getActionMasked() == 6) {
      event_remove = event.getActionIndex();
      timer = 0;
    }

    for (int i = 0; i < touch_events; i++) {
      if (fingers.size() > 0) {
        fingers.get(i).x = event.getX(i);
        fingers.get(i).y = event.getY(i);
      }
    }
    if (event_remove != -1 && fingers.size() > 0) {
      fingers.remove(event_remove);
    }
  } else {
    if (event.getActionMasked() == 0 || event.getActionMasked() == 5) {
      fingers.clear();
      larger_radius = outer_radius;
      timer = 0;
      chosen = -1;
      return false;
    }
  }
  return super.surfaceTouchEvent(event);
}
